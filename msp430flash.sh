#!/bin/sh

# MSP430 BootStrapLoader (BSL) init script.
# This script applies the appropriate entry sequence on the RST and
# TEST pins to force the MSP430 to start execution at the BSL RESET
# vector.
#
# GPIO/Pin mapping (on connector J7):
# +---------------------------+
# | GPIO | PIN | Desc.        |
# +---------------------------+
# |  168 |   7 | JTAG_RST_1   |
# |  169 |   8 | JTAG_TEST_1  |
# |  194 |   5 | ENABLE_BSL_1 |
# +---------------------------+
#
# Note:
# * Set BSLENA pin to 0 before anything else.
# * The RST pin is active-low
# * The TEST pin is active-high
#
# For more informations:
# http://www.ti.com/lit/ug/slau319i/slau319i.pdf
#

RSTGPIO="168"
TESTGPIO="169"
BSLENAGPIO="194"
RST="/sys/class/gpio/gpio${RSTGPIO}/value"
TEST="/sys/class/gpio/gpio${TESTGPIO}/value"
BSLENA="/sys/class/gpio/gpio${BSLENAGPIO}/value"

echo $BSLENAGPIO > /sys/class/gpio/export
echo $TESTGPIO > /sys/class/gpio/export
echo $RSTGPIO > /sys/class/gpio/export

echo "Setting BSL_ENA pin to 0..."
echo out > /sys/class/gpio/gpio${BSLENAGPIO}/direction
echo 0 > $BSLENA
sleep 1s

echo out > /sys/class/gpio/gpio${RSTGPIO}/direction
echo out > /sys/class/gpio/gpio${TESTGPIO}/direction

echo "Charge capacitor on boot loader hardware..."
echo 1 > $RST
echo 0 > $TEST
sleep 0.25s # charge capacitor on boot loader hardware

echo "Applying entry sequence to MSP430..."
echo 0 > $RST
echo 1 > $TEST
echo 0 > $TEST
echo 1 > $TEST
echo 0 > $TEST
echo 1 > $RST
echo 1 > $TEST
sleep 0.25s # give MSP430's oscillator time to stabilize

# TODO:
# call python script to erase main memory
# call python script to program fw
# msp430-bsl5-uart -v -v -v -v --no-start -e --port /dev/ttySC0
# msp430-bsl5-uart -v -v -v -v --no-start -p --port /dev/ttySC0 flash.txt
echo "Done!"
exit 0
