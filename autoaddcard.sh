#!/bin/sh

FILE="$1"

# dos2unix $FILE

read -p "Adresse IP de la borne: " ip

COUNT=0
while read line
do
    wget "http://${ip}:8080/cardaddform_req?card=${line}&repeat=0&group_id="
    COUNT=$((COUNT + 1))
done < $FILE

rm cardaddform_re*

echo "${COUNT} cartes envoyees vers la borne"

# wget "http://${IP}:8080/cardaddform_req?card=1069290188728&repeat=0&group_id="

exit 0
