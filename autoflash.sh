#!/usr/bin/expect
set timeout 60
set prompt "CCARDMX28"
set version "coreplus/0.3.18.7"
#set version "linux-digi"
#set prompt "CCARDWMX28"
#zset prompt "CCARDIMX28"
spawn picocom -b 115200 /dev/ttyUSB0
expect -- "Hit any key to stop autoboot" {
    sleep 0.1
    send "\r\n" }

expect $prompt { send "setenv serverip 192.168.1.23\r" }
expect $prompt { send "setenv dhcp on\r" }
expect $prompt { send "setenv loadbootsc off\r" }
expect $prompt { send "saveenv\r" }
expect $prompt { send "\r" }
expect $prompt { send "update linux tftp $version/uImage-ccardimx28js\r" }
expect $prompt { send "\r" }
expect $prompt { send "update rootfs tftp $version/rootfs-ccardimx28js-128.jffs2\r" }
expect $prompt { send "\r" }
expect $prompt { puts "done\n" }
sleep 5
